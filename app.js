let n = document.querySelector("#n");
let m = document.querySelector("#m");

let targetNumber = Math.floor(Math.random()*100);

document.addEventListener('keydown',(e)=>{
    if(e.keyCode == 13){
        let guessNumber = parseInt(n.value);
        if(guessNumber == targetNumber){
            document.body.innerHTML = "<h1>You won!</h1>";
        }else if(guessNumber < targetNumber){
            m.textContent = "Try a higer number.";
        }else if(guessNumber > targetNumber){
            m.textContent = "Try a lower number.";
        }
        n.value = "";
    }
});